const express = require('express');
const bodyParser = require('body-parser');
const cookieParser = require('cookie-parser');
const compress = require('compression');
const methodOverride = require('method-override');
const cors = require('cors');
const routes = require('../routes/index.route');
const salesManager = require('../controllers/sales.controller');
const app = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(cookieParser());
app.use(compress());
app.use(methodOverride());
app.use(cors());

/**
 *  This is where we set the store variable that should be on every lambda endpoint
 */
app.use('/', routes);

/**
 * Default error response
 */
app.use('*', (req, res, next) => {
	// for cloud watch scripts there isn't a route so we have to read the data from the header
	if (req.headers['x-apigateway-event']) {
		let data = JSON.parse(decodeURIComponent(req.headers['x-apigateway-event']));

		// let's also add the parameters to the request
		req.params = data;
		console.log('run cron', req.params);
		salesManager.processSale(req, res);
	} else {
		res.status(400);
		res.send({
			error: true,
			protocol: req.protocol,
			host: req.get('host'),
			url: req.originalUrl,
			message: 'redBergs'
		})
	}
});

module.exports = app;
