const express = require('express');
const salesCtrl = require('../controllers/sales.controller');

const router = express.Router();

router.route('/:store/process')
	.post(salesCtrl.processSale);

module.exports = router;
