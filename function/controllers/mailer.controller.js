const mailer = require('../services/mailer.service');
const request = require('../services/request.service');
const config = require('../config/config');
const moment = require('moment');
const fs = require('fs');
const appRoot = require('app-root-path');
// this controller will be responsible for creating the mailer templates and objects
let ribon_mail = false;

async function init(store) {
	ribon_mail = await new mailer(store);
};

async function buildMailObject(store, order, order_shipping, products, condensed) {
	const minibc = { 
		orderid: order.id.toString(), 
		storeID: config[store.store].minibc_storeID, 
		token: config[store.store].minibc_token
	};

	let card_details = await request.get(`${config[store.store].minibc}/apps/qehome/storefront/cardDetails`, minibc, {}, true);
	let mailer_object = {
	    "dictionary": {
	        'order_id'						: order.id.toString(),
			'order_date'					: moment(order.date_created).format('YYYY/MM/DD'),
			'order_comment'					: order.customer_message,

			// customer information
			'customer_email' 				: order.billing_address.email,
			'customer_phone' 				: order.billing_address.phone,
			'customer_fName' 				: order.billing_address.first_name,
			'customer_lName'				: order.billing_address.last_name,
			'customer_fName_upper'			: order.billing_address.first_name,
			'customer_lName_upper'			: order.billing_address.last_name,

			// billing address
			'baddress_to' 					: order.billing_address.email,
			'baddress_street' 				: order.billing_address.street_1 + ' ' + order.billing_address.street_2,
			'baddress_city' 				: order.billing_address.city,
			'baddress_state'				: order.billing_address.state,
			'baddress_country_code'			: order.billing_address.country_iso2,
			'baddress_zip'					: order.billing_address.zip,

			// shipping address
			'saddress_to' 					: order_shipping.first_name + ' ' + order_shipping.last_name,
			'saddress_street' 				: order_shipping.street_1 + ' ' + order_shipping.street_2,
			'saddress_city' 				: order_shipping.city,
			'saddress_state'				: order_shipping.state,
			'saddress_country_code'			: order_shipping.country_iso2,
			'saddress_zip'					: order_shipping.zip,
			'saddress_method'				: order_shipping.shipping_method,
			'saddress_email'				: order_shipping.email,
			'saddress_phone'				: order_shipping.phone,

			'item_html_list'				: await buildItems(products, condensed),
			'item_cnt'						: products.length.toString(),

			'total_tax'						: parseFloat(order.total_tax).toFixed(2),
			'discount_amount'				: '&#x24;' + parseFloat(order.coupon_discount).toFixed(2),
			'shipping_cost_inc_tax' 		: parseFloat(order_shipping.cost_ex_tax).toFixed(2),
			'subtotal_inc_tax'				: parseFloat(order.subtotal_ex_tax).toFixed(2),
			'total_inc_tax' 				: parseFloat(order.total_inc_tax).toFixed(2),
			'store_credit'					: '&#x24;' + parseFloat(order.store_credit_amount).toFixed(2),
			'payment_method' 				: order.payment_method,
			'card_type'						: card_details.card_type,
			'lst_4_digit'					: card_details.lst_4_digit,

			'store_url'						: store.secure_url,
			'subject'						: 'Your QE Home Order Confirmation (#' + order.id + ')',
			'qr_code'						: await createHTML('qrcode.link.html', order.qr)
	    },
	    "to": {
	    	"qetesting@mailinator.com": "Testing" 
	    },
	    "tags": {
	        "store": store.store,
	        "action": "qe_order_placed"
	    },
	    "webhook": "order_complete"
	}

	// console.log('mailer object', JSON.stringify(mailer_object));

	return mailer_object;
}

async function buildItems(products, condensed) {
	let html = "";

	for(let p in products) {
		// let's create the object we want to replace all the variables for
		const prod = products[p];
		let product_options = "";

		prod.product_options.forEach(option => {
			product_options += option.display_name + ': ' + option.display_value + '<br>';
		});

		let vars = {
			first_style: "",
			image_url: prod.image_url,
			brand: prod.brand_name,
			name: prod.name,
			barcode: prod.sku,
			product_option_html: product_options,
			final_sale: "",
			total: parseFloat(prod.total_ex_tax).toFixed(2),
			quantity: prod.quantity,
			shop_button: ""
		}

		if (p == 0) vars.first_style = "border-top-color:#cbcccd;border-top-style:solid;border-top-width:1px";
		
		let template = condensed ? 'order.item.condensed.html' : 'order.item.html';
		html += await createHTML(template, vars);
	}

	html = html.replace(/(\s+)/g, ' ');

	// console.log('html', html);

	return html;
}

async function createHTML(template_name, vars) {
	// get the template
	let file = await fs.readFileSync(`${appRoot.path}/function/templates/${template_name}`, 'utf-8');
	// just sometimes for styles
	file = file.replace(/{{(\s*)?([A-Za-z_]*)(\s*)?}}/g, '{{$2}}');
	for(let v in vars) {
		let match = '{{' + v + '}}';
		let replace = vars[v];
		let re = new RegExp(match, "g");
		file = file.replace(re, replace);
	}

	// console.log('file', file);

	return file;
}

async function sendMail(object) {
	return await ribon_mail.sendMail(object);
}

function MailerClass(store) {
	return init(store).then(() => {
		return {
			buildMailObject: buildMailObject,
			sendMail: sendMail
		}
	});
}

module.exports = MailerClass;
