const request = require('../services/request.service');
const mailer = require('./mailer.controller');
const pos = require('./pos.controller');
const data = require('../services/data.service');
const config = require('../config/config');
const moment = require('moment');

// variables
let headers = {
	"Accept": "application/json",
	"Content-Type": "application/json"
}

let ribon_data = false;

async function processOrder(req, res) {
	// if we trigger this via webhook then we have to get the orderid from the body instead of the parameter
	// start the logged response here and even if there's an error, we can update it with things that have completed and that which hasn't
	ribon_data = await new data(req.params.store);

	let orderid = req.params.id;
	if (orderid == 'webhook') {
		orderid = req.body.data.id;
		// when we start we have to wait a few seconds because sometimes the webhook fires too quickly
		let ebridge = await ribon_data.getData('ebridge_discounts', { order_id: orderid, debug: 'bapo'});

		// let's clean the ebridge object
		if (ebridge instanceof Array && ebridge.length > 0) {
			// this means that the record exists and the order has been completed
		} else {
			const random_time = Math.floor(Math.random() * 20) + 10;
			await timeout(random_time * 1000);
		}
	}
	let log;

	console.log('post data', req.params, req.body);
	try {
		// first thing we do is get all the necessary information we need to process the order
		let info = await getOrderInformation(req.params.store, orderid);

		if (info.order.status_id == 0) {
			throw('Incomplete order');
		}

		let order_log_data = await ribon_data.getData('order_logs', { order_id: orderid, debug: 'bapo'});
		console.log('order_log_data', order_log_data);
		
		// if the log exists already
		if (order_log_data instanceof Array && order_log_data.length > 0) {
			if (order_log_data[0].order_id == orderid) {
				log = order_log_data[0];

				if (log.processing == 1) {
					console.log('processing order already', orderid);
					throw('Order Already Processing');
				}
			} else {
				log = {
					order_id: orderid,
					qr_url: '',
					ship_out_date: '',
					est_arrival_date: '',
					status: '',
					warehouse_email_sent: 0,
					confirmation_sent: 0,
					ebridge_result: 0,
				}
			}
		} else {
			log = {
				order_id: orderid,
				qr_url: '',
				ship_out_date: '',
				est_arrival_date: '',
				status: '',
				warehouse_email_sent: 0,
				confirmation_sent: 0,
				ebridge_result: 0,
			}
		}

		// if it get's this far let's update the log with processing so we don't have duplicates being processed at the same time
		log.processing = 1;
		await updateLog(log);
		
		console.log("what does the log start off with?", JSON.parse(JSON.stringify(log)));
		// we ca do a POS push update at a later time to keep this all consistent
		let POS = await new pos(req.params.store);

		if (log.ebridge_result == 0) {
			console.log('sending to ebridge');
			info.ebridge = await POS.sendToEbridge(info);
		}

		if (info.ebridge) {
			log.ebridge_result = 1;
			await updateLog(log);
		}

		let ribon_mail = await new mailer(req.params.store);

		// process ship to store if needed
		let store_email = false;
		info.order.qr = {url: '',qr_url: ''};
		if (info.order_shipping.shipping_method.toLowerCase().indexOf('ship to') > -1) {
			// set the qr object so that it can be put in the mailer and the database
			if (log.qr_url == '') {
				info.order.qr = await POS.generateQRCode(info.store, info.order.id, info.order.customer_id, info.order_shipping.shipping_method.replace(/^Ship to: (.*)\(([0-9]*)(.*)$/ig, '$2'));
				console.log('qr info', info.order.qr);
				log.qr_url = info.order.qr.qr_url;
				log.ship_out_date = '0000-00-00';
				log.est_arrival_date = '0000-00-00';
				log.status = 'Placed';
			} else {
				// in this case we probably have to update the dates and times
				info.order.qr = {
					qr_url: log.qr_url
				}
			}

			// send an email to the warehouse
			if (log.warehouse_email_sent == 0) {
				// while we're here let's also insert the qr code into the minibc database
				let post_minibc = {
					store_id: info.order_shipping.shipping_method.replace(/^Ship to: (.*)\(([0-9]*)(.*)$/ig, '$2'),
					order_id: info.order.id,
					customer_id: config[req.params.store].minibc_store_number,
					user_id: info.order.customer_id,
					qr_url: info.order.qr.qr_url,
					user_name: `${info.order.billing_address.first_name} ${info.order.billing_address.last_name}`,
					user_email: info.order.billing_address.email,
					ship_out_date: '0000-00-00',
					est_arrival_time: '0000-00-00',
					status: 'Placed',
					storeID: config[req.params.store].minibc_storeID,
					token: config[req.params.store].minibc_token
				}
				let mbc = await request.post(`${config[req.params.store].minibc}/apps/qehome/storefront/saveSTS`, post_minibc, {}, true);
				console.log('mbc', mbc);

				if (!mbc.success) {
					throw('Unable to save STS information');
				}

				let mailer_object = await ribon_mail.buildMailObject(info.store, info.order, info.order_shipping, info.order_products, true);
				// we have to populate the correct to address and subject
				mailer_object.tags.action = 'qe_order_placed_office';
				mailer_object.dictionary.subject = `Order Confirmation: ${info.order.id}`;

				// we have to get the email address from the database
				let store_data = await ribon_data.getData('transit_times', { store_id: info.order_shipping.shipping_method.replace(/^Ship to: (.*)\(([0-9]*)(.*)$/ig, '$2'), debug: 'bapo' });
				
				store_email = 'robin+warehouse@beapartof.com';
				if (store_data.length > 0) {
					store_email = store_data[0].store_email;
				}

				// console.log('warehouse', warehouse_data, warehouse_email);

				// reset the to object - keep this as a testing warehouse for now
				mailer_object.to = {};
				// mailer_object.to[store_email] = 'QE Home';
				mailer_object.to['weborder@qehomedecor.com'] = 'QE Home';
				
				let wh = await ribon_mail.sendMail(mailer_object);

				// we have to send a second mail to weborder@qehomedecor.com which is the qr code without the product images
				// mailer_object = await ribon_mail.buildMailObject(info.store, info.order, info.order_shipping, info.order_products, true);

				// // adjustments
				// mailer_object.tags.action = 'qe_order_placed_office';
				// mailer_object.dictionary.subject = `Order Confirmation: ${info.order.id}`;
				// mailer_object.to = {};
				// mailer_object.to['weborder@qehomedecor.com'] = 'QE Home';

				// let wh2 = await ribon_mail.sendMail(mailer_object);

				if (wh) {
					log.warehouse_email_sent = 1;
					await updateLog(log);
				}
			}
		}

		// send out the emails
		// send an email to the customer
		console.log('send customer email?', JSON.parse(JSON.stringify(log)));
		if (log.confirmation_sent == 0) {
			let mailer_object = await ribon_mail.buildMailObject(info.store, info.order, info.order_shipping, info.order_products, false);

			// reset the to object
			mailer_object.to = {};
			mailer_object.to[info.order.billing_address.email] = `${info.order.billing_address.first_name} ${info.order.billing_address.last_name}`;
			mailer_object.to['weborder@qehomedecor.com'] = 'QE Home';

			if (store_email)
				mailer_object.to[store_email] = 'QE Home';

			// customer shouldn't see the qr code
			mailer_object.dictionary.qr_code = '';

			let mr = await ribon_mail.sendMail(mailer_object);

			console.log('mr', mr);

			if (mr) {
				log.confirmation_sent = 1;
				await updateLog(log);
			}
		}

		// verify that the order has all the correct numbers and matches what's in BC

		// log the results of each step of the order
		// we have to log the responses for the qr information as well as mailer results and POS information
		// we should already have the discount information, so we can also compare any price discrepancies here as well

		// the last thing we do it update the database
		log.processing = 0;
		await updateLog(log);
		
		// res.send(info);
		res.send(log);
	}
	catch (e) {
		// catch the error
		console.log('error', e);
		// on error, let's log the entry so later we can see what went wrong
		log.processing = 0;
		await updateLog(log);

		res.status(401);
		res.send({
			error: true,
			message: typeof e === 'string' ? e : 'Unable to complete the transaction'
		});
	}
}

async function updateLog(log) {
	let update = false;

	let order_log_data = await ribon_data.getData('order_logs', { order_id: log.order_id, debug: 'bapo'});
		
	// if the log exists already
	if (order_log_data instanceof Array && order_log_data.length > 0) {
		log.id = order_log_data[0].id;
	}

	if (log.id) {
		// let's update the entry
		let query = JSON.parse(JSON.stringify(log));
		delete query.id;
		delete query.created_at;
		delete query.updated_at;
		update = await ribon_data.updateData('order_logs', log.id, query);
		console.log('update log', query, log.id);
	} else {
		// we have to create the record
		update = await ribon_data.postData('order_logs', log);
		console.log('insert log ', log);
	}

	return update;
}

function timeout(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
}

/**
 * Let's put in some helper functions so we can organize this better
 */
async function getOrderInformation(hash, orderid) {
	let store = await request.post(`${config.ribon}/api/apps/getStoreSettings`, { store: hash }, {}, true);
		
	// let's get the order from bigcommerce
	headers['X-Auth-Client'] = store.client_id;
	headers['X-Auth-Token'] = store.token;

	// let's get the order and the products
	let order = await request.get(`https://api.bigcommerce.com/stores/${hash}/v2/orders/${orderid}`, {}, headers, true);
	let order_products = await request.get(`https://api.bigcommerce.com/stores/${hash}/v2/orders/${orderid}/products`, {}, headers, true);
	let order_shipping = await request.get(`https://api.bigcommerce.com/stores/${hash}/v2/orders/${orderid}/shippingaddresses`, {}, headers, true);
	let ids = [];
	order_products.forEach(prod => {
		ids.push(prod.product_id);
	})
	ids = ids.join(',');

	let product_details = await request.get(`https://api.bigcommerce.com/stores/${hash}/v3/catalog/products?id:in=${ids}&include=images`, {}, headers, true);
	let brands = await request.get(`https://api.bigcommerce.com/stores/${hash}/v3/catalog/brands`, {}, headers, true)

	let brand_index = {};
	brands.data.forEach(brand => {
		brand_index[brand.id] = brand;
	});

	for(let p in product_details.data) {
		const prod = product_details.data[p];
		for(let o in order_products) {
			const op = order_products[o];
			console.log('ids', op.product_id == prod.id, op.product_id, prod.id)
			if (op.product_id == prod.id) {
				op.details = prod;
				op.brand_name = brand_index[prod.brand_id] ? brand_index[prod.brand_id].name : "";
				
				// console.log('images', prod.images);
				for(let i in prod.images) {
					const image = prod.images[i];
					// console.log('image', image);
					if (image.is_thumbnail === true) {
						op.image_url = image.url_thumbnail;
					}
				}
			}
		};
	}

	if (order_shipping.length === 1) {
		order_shipping = order_shipping[0];
	} else {
		// error getting shipping
		throw('Unable to get Shipping information');
	}

	return {
		store: store,
		order: order,
		order_products: order_products,
		order_shipping: order_shipping
	}
}

module.exports.processOrder = processOrder;