const request = require('../services/request.service');
const config = require('../config/config');

// variables
let headers = {
	"Accept": "application/json",
	"Content-Type": "application/json"
}
let storeDataToken = '';

async function getStoreToken() {
    try {
        const storeToken = await request.get(`${config.ribon}/api/store/${config.storeHash}/services/tokens`, {}, {}, true);

        return storeToken[0].token;
    }
    catch (error) {
        // catch the error
        console.log('Error', error === 'string' ? error : 'Unable to complete getStoreToken');
	}
}

async function processSale(req, res) {
    try {
        storeDataToken = await getStoreToken();

        headers['token'] = storeDataToken;
        const salesManager = await request.get(`${config.ribon_data}/api/v1/data/sales_manager/`, {}, headers, true);
        salesManager.sort(shortByDate);

        let message = [];
        const currentTimeStamp = new Date().getTime();
        const yesterTimeStamp = new Date().getTime() - 86400000;

        for (let index in salesManager) {
            const salesManagerItem = salesManager[index];
            // console.log(salesManagerItem.start, '/', salesManagerItem.end, '/', new Date());
            let messageTxt;
            const startTimeStamp = new Date(salesManagerItem.start.replace(' ', 'T')).getTime();
            const endTimeStamp = new Date(salesManagerItem.end.replace(' ', 'T')).getTime();
            const after30days = endTimeStamp + (30 * 24 * 60 * 60 * 1000);
            // status === 1 means sales information added without actual product data
            if (salesManagerItem.status === '1') {
                // console.log(salesManagerItem);
                // Get only current date sales
                if (startTimeStamp > yesterTimeStamp && startTimeStamp < currentTimeStamp && salesManagerItem.status === '1') {
                    // console.log(productInfo);
                    const productInfo = await getProductData(salesManagerItem.sku);
                    if (productInfo) {
                        await insertSalesBulkPricingRules(productInfo);
                        await insertSalesProducts(productInfo, salesManagerItem);
                        await updateSaleStatus(salesManagerItem.id, 2);
                        console.log('Complete updating sales product data for ' + salesManagerItem.sku + ' (' + salesManagerItem.salename + ')');
                        messageTxt = 'Complete updating sales product data for ' + salesManagerItem.sku + ' (' + salesManagerItem.salename + ')';
                    } else {
                        // error
                        console.log('No updating sales product data for ' + salesManagerItem.sku + ' (' + salesManagerItem.salename + ')');
                        messageTxt = 'No updating sales product data for ' + salesManagerItem.sku + ' (' + salesManagerItem.salename + ')';
                    }

                } else {
                    console.log('Pending sales for ' + salesManagerItem.sku + ' (' + salesManagerItem.salename + ')');
                    messageTxt = 'Pending sales for ' + salesManagerItem.sku + ' (' + salesManagerItem.salename + ')';
                }
            } else if (salesManagerItem.status === '2') {
                if (startTimeStamp < currentTimeStamp && endTimeStamp < currentTimeStamp) {
                    await restoreOriginProductData(salesManagerItem);
                    console.log('Complete restoring Origin data sales for ' + salesManagerItem.sku + ' (' + salesManagerItem.salename + ')');
                    messageTxt = 'Complete restoring Origin data sales for ' + salesManagerItem.sku + ' (' + salesManagerItem.salename + ')';
                } else {
                    console.log('The current running sales for ' + salesManagerItem.sku + ' (' + salesManagerItem.salename + ')');
                    messageTxt = 'The current running sales for ' + salesManagerItem.sku + ' (' + salesManagerItem.salename + ')';
                }
            } else {
                if (after30days < currentTimeStamp) {
                    // Delete sales product info from sales_manager by its own id
                    await request.delete(`${config.ribon_data}/api/v1/data/sales_manager/` + salesManagerItem.id, {}, headers, true);
                    console.log('Complete deleting sales data for ' + salesManagerItem.id + '-' + salesManagerItem.sku + ' (' + salesManagerItem.salename + ')');
                    messageTxt = 'Complete deleting sales data for ' + salesManagerItem.id + '-' + salesManagerItem.sku + ' (' + salesManagerItem.salename + ')';
                } else {
                    console.log('The ended sales for ' + salesManagerItem.sku + ' (' + salesManagerItem.salename + ') but nothing to delete.');
                    messageTxt = 'The ended sales for ' + salesManagerItem.sku + ' (' + salesManagerItem.salename + ') but nothing to delete.';
                }
            }

            message.push(messageTxt);
        }

        res.send(message);
    }
    catch (error) {
        // catch the error
        console.log('Error', error === 'string' ? error : 'Unable to complete the process');
        res.send({
            error: true
        });
	}
}

async function getBackupProductData(sku) {
    try {
        headers['token'] = storeDataToken;
        const productData = await request.get(`${config.ribon_data}/api/v1/data/sales_products`, { sku: sku }, headers, true);

        return productData;
    }
    catch (error) {
		// catch the error
        console.log('Error', error === 'string' ? error : 'Unable to complete getBackupProductData');
        return false;
	}
}

async function getBackupBulkPricingRules(sku) {
    try {
        headers['token'] = storeDataToken;
        const bulkPricingRulesData = await request.get(`${config.ribon_data}/api/v1/data/sales_products_bulk_pricing_rules`, { sku: sku }, headers, true);

        return bulkPricingRulesData;
    }
    catch (error) {
		// catch the error
        console.log('Error', error === 'string' ? error : 'Unable to complete getBackupBulkPricingRules');
        return false;
	}
}

async function getProductData(sku) {
    const product_details = await request.post(`${config.ribon}/api/live/${config.storeHash}/get`, {
        uri: '/v3/catalog/products?sku=' + sku
    }, {}, true);
    switch (true) {
        case product_details.data.length === 0:
        case product_details.data.length > 1:
            console.log('Need to handle an error for more than one item or no item');

            return false;
        default:
            const bulk_pricing_rules = await request.post(`${config.ribon}/api/live/${config.storeHash}/get`, {
                uri: '/v3/catalog/products/' + product_details.data[0].id + '/bulk-pricing-rules'
            }, {}, true);

            const data = {
                product_id: product_details.data[0].id,
                sku: product_details.data[0].sku,
                price: product_details.data[0].price,
                categories: product_details.data[0].categories.join(','),
                bulkPricingRules: bulk_pricing_rules.data
            };

            return data;
    }
}

async function insertSalesProducts(data, salesManagerData) {
    try {
        console.log('insertSalesProducts', data.sku);
        headers['token'] = storeDataToken;

        // Delete unnecessary data
        delete data.bulkPricingRules;

        // Backup actual product information to sales_products
        await request.post(`${config.ribon_data}/api/v1/data/sales_products`, data, headers, true);
        console.log('sales_products for ', data.sku);

        // Update original product table in bigCommerce with sales' info
        await updateOriginProduct(data, salesManagerData.price);

        return true;
    }
    catch (error) {
        // catch the error
        console.log('Error', error === 'string' ? error : 'Unable to complete insertSalesProducts');
        return false;
	}
}

async function insertSalesBulkPricingRules(data) {
    try {
        console.log('insertSalesBulkPricingRules', data.sku, typeof data.bulkPricingRules);
        // Only bulk Pricing Rule existed
        if (typeof data.bulkPricingRules !== 'undefined') {
            headers['token'] = storeDataToken;
            let newData = {};
            for (let index in data.bulkPricingRules) {
                newData = {
                        product_id: data.product_id,
                        sku: data.sku,
                        quantity_min: data.bulkPricingRules[index].quantity_min,
                        quantity_max: data.bulkPricingRules[index].quantity_max,
                        type: data.bulkPricingRules[index].type,
                        amount: data.bulkPricingRules[index].amount
                };

                await request.post(`${config.ribon_data}/api/v1/data/sales_products_bulk_pricing_rules`, newData, headers, true);
                console.log('sales_products_bulk_pricing_rules for ', data.sku);
            }

            await updateOriginBulkPricingRule(data);
        }

        return true;
    }
    catch (error) {
        // catch the error
        console.log('Error', error === 'string' ? error : 'Unable to complete insertSalesBulkPricingRules');
        return false;
	}
}

async function updateSaleStatus(salesManagerId, statusId) {
    try {
        headers['token'] = storeDataToken;
        // statusId 0 = Ended
        // statusId 1 = new entry
        // statusId 2 = Activate
        // Update the status as 2 in Sales Manager, when actual product info has updated
        // It will prevent backup product data more than once
        await request.post(`${config.ribon_data}/api/v1/data/sales_manager/` + salesManagerId, { status: statusId }, headers, true);
        console.log('updateSaleStatus for ', salesManagerId);

        return true;
    }
    catch (error) {
		// catch the error
        console.log('Error', error === 'string' ? error : 'Unable to complete updateSaleStatus');
        return false;
	}
}

async function updateOriginProduct(data, salePrice) {
    try {
        // let's get things from bigCommerce
        headers['X-Auth-Client'] = config.client_id;
        headers['X-Auth-Token'] = config.token;

        let newCategories = ['31'];
        const categoryArray = data.categories.split(',');
        categoryArray.forEach(element => {
            if (newCategories.indexOf(element) === -1) {
                newCategories.push(element);
            }
        });

        const newProductObject = {
            sale_price: salePrice,
            categories: newCategories
        };

        await request.post(`${config.ribon}/api/live/${config.storeHash}/put`, {
            uri: '/v3/catalog/products/' + data.product_id,
            query: newProductObject
        }, headers, true);

        console.log('updateOriginProduct for ', data.product_id);
        return true;
    }
    catch (error) {
		// catch the error
        console.log('Error', error === 'string' ? error : 'Unable to complete updateOriginProductData');
        return false;
	}
}

async function updateOriginBulkPricingRule(data) {
    try {
        // let's get things from bigCommerce
        headers['X-Auth-Client'] = config.client_id;
        headers['X-Auth-Token'] = config.token;

        if (typeof data.bulkPricingRules !== 'undefined') {
            for (let index in data.bulkPricingRules) {
                await request.post(`${config.ribon}/api/live/${config.storeHash}/delete`, {
                    uri: '/v3/catalog/products/' + data.product_id + '/bulk-pricing-rules/' + data.bulkPricingRules[index].id,
                }, headers, true);
                console.log('updateOriginBulkPricingRule for ', data.product_id);
            }
        }

        return true;
    }
    catch (error) {
		// catch the error
        console.log('Error', error === 'string' ? error : 'Unable to complete updateOriginBulkPricingRule');
        return false;
	}
}

async function restoreOriginProductData(data) {
    try {
        const backupProduct = await getBackupProductData(data.sku);
        const backupBulkPricingRules = await getBackupBulkPricingRules(data.sku);
        // let's get things from bigCommerce
        headers['X-Auth-Client'] = config.client_id;
        headers['X-Auth-Token'] = config.token;
        headers['token'] = storeDataToken;

        if (backupProduct.length > 0) {
            let categoryArray = [];
            if (typeof backupProduct[0].categories !== 'undefined') {
                categoryArray = backupProduct[0].categories.split(',');
                const index = categoryArray.indexOf('31');
                if (index > -1) {
                    categoryArray.splice(index, 1);
                }
            }

            const originProductObject = {
                price: backupProduct[0].price,
                sale_price: 0,
                categories: categoryArray
            };

            // reStore original product info from backup data
            await request.post(`${config.ribon}/api/live/${config.storeHash}/put`, {
                uri: '/v3/catalog/products/' + backupProduct[0].product_id,
                query: originProductObject
            }, headers, true);
            console.log('reStore original product for ', backupProduct[0].product_id);

            // Delete product data from sales_products by its own id
            const backProduct =await request.delete(`${config.ribon_data}/api/v1/data/sales_products/` + backupProduct[0].id, {}, headers, true);
            console.log('delete backup product for ', backupProduct[0].product_id, backProduct);
        }

        if (backupBulkPricingRules.length > 0) {
            for (let index in backupBulkPricingRules) {
                const originBulkPricingRules = {
                    quantity_min: backupBulkPricingRules[index].quantity_min,
                    quantity_max: backupBulkPricingRules[index].quantity_max,
                    type: backupBulkPricingRules[index].type,
                    amount: backupBulkPricingRules[index].amount
                };

                let product_id;
                if (backupProduct.length < 1) {
                    product_id = backupBulkPricingRules[index].product_id;
                } else {
                    product_id = backupProduct[0].product_id;
                }
                // reStore original product bulk pricing rules info from backup data
                await request.post(`${config.ribon}/api/live/${config.storeHash}/post`, {
                    uri: '/v3/catalog/products/' + product_id + '/bulk-pricing-rules',
                    query: originBulkPricingRules
                }, headers, true);
                console.log('reStore original bulk rule for ', product_id);

                // Delete product bulk pricing rule data from sales_products_bulk_pricing_rules by its own id
                const backBulkRule = await request.delete(`${config.ribon_data}/api/v1/data/sales_products_bulk_pricing_rules/` + backupBulkPricingRules[index].id, {}, headers, true);
                console.log('delete backup bulk rule for ', backupBulkPricingRules[index].id, backBulkRule);
            }
        }

        await updateSaleStatus(data.id, 0);

        return true;
    }
    catch (error) {
		// catch the error
        console.log('Error', error === 'string' ? error : 'Unable to complete restoreOriginProductData for ' + data.sku);
        return false;
	}
}

function shortByDate(a, b) {
    const genreA = a.start;
    const genreB = b.start;

    let comparison = 0;
    if (genreA > genreB) {
      comparison = 1;
    } else if (genreA < genreB) {
      comparison = -1;
    }

    return comparison;
}

module.exports = {
    processSale: processSale
};
