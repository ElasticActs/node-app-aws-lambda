const request = require('../services/request.service');
const ebridge_service = require('../services/ebridge.service');
const data = require('../services/data.service');
const config = require('../config/config');
const appRoot = require('app-root-path');
const xmljs = require('xml-js');
const qrcode = require('qrcode');
const fs = require('fs');
const { exec } = require('child_process');
const uuid = require('uuid/v4');
const moment = require('moment');

let connection = false;

async function init() {
	// let's connect to the soap client
	// connection = await new ebridge();
	return true;
}

async function generateQRCode(store, order_id, customer_id, store_id) {
	// this should create a QR code and upload it to the store
	return new Promise((resolve, reject) => {
		qrcode.toDataURL(`${store.secure_url}/ship-to-store?cid=${customer_id}&oid=${order_id}&sid=${store_id}`, (err, url) => {
			// console.log('qrurl', err, url);
			// first let's convert the tring to an image we can send to the store
			let base64Data = url.replace(/^data:image\/png;base64,/, "");
			
			let name = `STSQRCode-${uuid()}.png`
			fs.writeFile(`/tmp/${name}`, base64Data, 'base64', (err) =>  {
				console.log('output', err);

				// now that we're written it to a temporary directory, let's send it to the server
				let folder = "/qr-code/";
				let folder_split = folder.replace(/(\/{1}?)$/g, '').replace(/^(\/{1}?)?(.*)/g, '$2').split('/');
				let file = {
					name: name
				}
				createFolders(folder_split, "", store, file).then(results => {;
					resolve({
						url: `${store.secure_url}/ship-to-store?cid=${customer_id}&oid=${order_id}&sid=${store_id}`,
						qr_url:  `${store.secure_url}/content/qr-code/${name}`
					});
				});
			});
		});
	});
}

async function sendToEbridge(info) {
	// first let's get some relevant data for the POS
	try {
		let ribon_data = await new data(info.store.store);
		let ebridge = await ribon_data.getData('ebridge_discounts', { order_id: info.order.id, debug: 'bapo'});

		// let's clean the ebridge object
		if (ebridge instanceof Array && ebridge.length > 0) {
			ebridge = ebridge[0];
			ebridge.cart = JSON.parse(ebridge.cart);
			ebridge.discounts = JSON.parse(ebridge.discounts);
			ebridge.cart_index = {};

			// info.order_products.forEach(item => {
			// 	ebridge.cart_index[item.product_id] = item
			// });
		} else {
			// before we stop the POS entry, we can try to recreate the array from the order if there are no discounts on the order
			if (parseFloat(info.order.discount_amount) === 0) {
				// let's try to recreate the ebrige object
				ebridge = {
					cart: [],
					discounts: [],
					cart_index: {}
				}

				info.order_products.forEach(product => {
					let prod = {
						product_id: product.product_id,
					    line_item_id: `${product.product_id}-${product.sku}`,
					    price_before_discount: parseFloat(product.price_ex_tax),
					    price_after_discount: parseFloat(product.price_ex_tax),
					    name: product.name,
					    sku: product.sku
					}
					for(let i = 0; i < product.quantity; i++) {
						ebridge.cart.push(prod);
					}
				});
			} else {
				// this is an error
				throw('Ebridge record does not exist in database');
			}
		}

		// we're going to create the object that needs to be sent to the POS and send it via MiniBC right now
		// using MiniBC so that less re-writing needs to be done
		let ebridge_products = [];

		// let's also get an idea of how many of each ebridge cart there is
		let ec_in = {}
		ebridge.cart.forEach(p => {
			if (!ec_in[p.line_item_id]) ec_in[p.line_item_id] = [];

			ec_in[p.line_item_id].push(p);
		})

		// let's create the products that need to be sent here
		let existing_line_items = [];
		for(let p in info.order_products) {
			// let prod = ebridge.cart[p]
			let product = JSON.parse(JSON.stringify(info.order_products[p]));

			// let's find a matching ebridge product
			const max = product.quantity;
			// because the quantities can be more than 1, we have to loop through them all and match them to respective discount prices
			let line_item_id = false;
			for(let n = 1; n <= max; n++) {
				let prod = false;
				for(let index in ebridge.cart) {
					const ep = ebridge.cart[index];
					if (ep.product_id == product.product_id && (line_item_id === false || (line_item_id === ep.line_item_id)) && ec_in[ep.line_item_id].length >= max) {
						prod = ep;
						line_item_id = ep.line_item_id;
						// console.log('ep', ep, index);
						// let's also delete this index because it has been used already
						delete ebridge.cart[index];
						break;
					}
				};

				product.quantity = 1;
				product.active_tax_rate = ebridge.active_tax_rate;
				// let' set some variables here
				if (prod.price_after_notax) {
					product.price_ex_tax = prod.price_after_notax;
					product.price_inc_tax = prod.price_after_discount;
					product.price_tax = (parseFloat(product.price_inc_tax) - parseFloat(product.price_ex_tax)).toFixed(2);
				} else {
					product.price_ex_tax = prod.price_after_discount;
					product.price_inc_tax = (parseFloat(prod.price_after_discount) * parseFloat(ebridge.active_tax_rate) + prod.price_after_discount).toFixed(2);
					product.price_tax = (parseFloat(product.price_inc_tax) - parseFloat(product.price_ex_tax)).toFixed(2);
				}

				product.total_ex_tax = product.price_ex_tax;
				product.total_inc_tax = product.price_inc_tax;
				product.total_tax = (parseFloat(product.price_inc_tax) - parseFloat(product.price_ex_tax)).toFixed(2);

				// console.log('prod', {
				// 	id: product.product_id,
				// 	name: product.name,
				// 	price: product.price_inc_tax,
				// 	total: product.total_inc_tax
				// })

				ebridge_products.push(product);
			}
		}

		// console.log('ebridge_products', ebridge_products.length, info.order_products.length);

		// for testing let's throw an error now
		// throw('Testing now');
		// return ebridge_products;

		// so once we have data, let's map the data to our map, then convert it to xml
		let map = await mapEbridgeOrder(info.order, info.order_shipping, ebridge_products);
		let xml = xmljs.json2xml(map, { compact: true });
		let response = false;
	
		// now let's try sending the 
		let es = await new ebridge_service();
		response = await es.postDocument(xml, `${info.order.id}.xml`);
		console.log('response', response);
		// map = await es.getDoc('75606855');
		
		if (response && response.SendFileResult) {
			return response.SendFileResult;
		} else {
			return false;
		}
	}
	catch(e) {
		console.log('pos error', e);
		return false;
	}

}

async function mapEbridgeOrder(order, order_shipping, products) {
	let file = await fs.readFileSync(`${appRoot.path}/function/templates/ebridge.map.order.json`, 'utf-8');
	file = JSON.parse(file);

	// now that we have the map, let's add the values into the object
	// we only change necessary items, leave the rest
	file.Order.OrderHeader.OrderNumber.BuyerOrderNumber._text = order.id;
	file.Order.OrderHeader.OrderIssueDate._text = moment(order.date_created).format('YYYY-MM-DD HH:mm:ss');
	file.Order.OrderHeader.OrderAllowancesOrCharges['core:AllowOrCharge']['core:TypeOfAllowanceOrCharge']['core:MonetaryValue']['core:MonetaryAmount']._text = parseFloat(order.shipping_cost_ex_tax).toFixed(2);
	file.Order.OrderHeader.OrderTaxReference['core:TaxAmount']._text = parseFloat(order.total_tax).toFixed(2);

	// Buyer Party
	file.Order.OrderHeader.OrderParty.BuyerParty['core:NameAddress']['core:Name1']._text = cleanInput(`${order.billing_address.first_name} ${order.billing_address.last_name}`);
	file.Order.OrderHeader.OrderParty.BuyerParty['core:PrimaryContact']['core:ContactName']._text = cleanInput(`${order.billing_address.first_name} ${order.billing_address.last_name}`);
	file.Order.OrderHeader.OrderParty.BuyerParty['core:PrimaryContact']['core:ListOfContactNumber']['core:ContactNumber'] = [
		{
			"core:ContactNumberValue": {
				_text: order.billing_address.phone,
			},
			"core:ContactNumberTypeCoded": {
				_text: 'TelephoneNumber'
			}
		},
		{
			"core:ContactNumberValue": {
				_text: order.billing_address.email,
			},
			"core:ContactNumberTypeCoded": {
				_text: 'EmailAddress'
			}
		}
	];

	// Ship to Party
	file.Order.OrderHeader.OrderParty.ShipToParty['core:PartyID']['core:Ident']._text = order_shipping.id;
	file.Order.OrderHeader.OrderParty.ShipToParty['core:NameAddress'] = {
        "core:ExternalAddressID": {},
        "core:Name1": {
            "_text": cleanInput(`${order_shipping.first_name} ${order_shipping.last_name}`)
        },
        "core:Street": {
            "_text": cleanInput(`${order_shipping.street_1} ${order_shipping.street_2}`)
        },
        "core:StreetSupplement1": {},
        "core:StreetSupplement2": {},
        "core:PostalCode": {
            "_text": order_shipping.zip
        },
        "core:City": {
            "_text": cleanInput(order_shipping.city)
        },
        "core:Region": {
            "core:RegionCoded": {
                "_text": "Other"
            },
            "core:RegionCodedOther": {
                "_text": order_shipping.state
            }
        },
        "core:Country": {
            "core:CountryCoded": {
                "_text": "Other"
            },
            "core:CountryCodedOther": {
                "_text": order_shipping.country_iso2
            }
        }
    }
    file.Order.OrderHeader.OrderParty.ShipToParty['core:PrimaryContact'] = {
    	"core:ContactID": {
            "core:Ident": {}
        },
        "core:ContactName": {
            "_text": cleanInput(`${order_shipping.first_name} ${order_shipping.last_name}`)
        },
        "core:ListOfContactNumber": {
            "core:ContactNumber": [{
                "core:ContactNumberValue": {
                	"_text": order_shipping.phone
                },
                "core:ContactNumberTypeCoded": {
                    "_text": "PhoneNumber"
                }
            }, {
                "core:ContactNumberValue": {
                    "_text": order_shipping.email
                },
                "core:ContactNumberTypeCoded": {
                    "_text": "EmailAddress"
                }
            }, {
                "core:ContactNumberValue": {
                    "_text": order_shipping.phone
                },
                "core:ContactNumberTypeCoded": {
                    "_text": "TelephoneNumber"
                }
            }]
        }
    }

    file.Order.OrderHeader.OrderParty.ShipToParty['core:Identifier']['core:Ident']._text = order_shipping.id;

    // Bill To Party
    file.Order.OrderHeader.OrderParty.BillToParty['core:NameAddress'] = {
        "core:ExternalAddressID": {},
        "core:Name1": {
            "_text": cleanInput(`${order.billing_address.first_name} ${order.billing_address.last_name}`)
        },
        "core:Street": {
            "_text": cleanInput(`${order.billing_address.street_1} ${order.billing_address.street_2}`)
        },
        "core:StreetSupplement1": {},
        "core:StreetSupplement2": {},
        "core:PostalCode": {
            "_text": order.billing_address.zip
        },
        "core:City": {
            "_text": cleanInput(order.billing_address.city)
        },
        "core:Region": {
            "core:RegionCoded": {
                "_text": "Other"
            },
            "core:RegionCodedOther": {
                "_text": order.billing_address.state
            }
        },
        "core:Country": {
            "core:CountryCoded": {
                "_text": "Other"
            },
            "core:CountryCodedOther": {
                "_text": order.billing_address.country_iso2
            }
        }
    }
    file.Order.OrderHeader.OrderParty.BillToParty['core:PrimaryContact'] = {
    	"core:ContactID": {
            "core:Ident": {}
        },
        "core:ContactName": {
            "_text": cleanInput(`${order.billing_address.first_name} ${order.billing_address.last_name}`)
        },
        "core:ListOfContactNumber": {
            "core:ContactNumber": [{
                "core:ContactNumberValue": {
                	"_text": order.billing_address.phone
                },
                "core:ContactNumberTypeCoded": {
                    "_text": "PhoneNumber"
                }
            }, {
                "core:ContactNumberValue": {
                    "_text": order.billing_address.email
                },
                "core:ContactNumberTypeCoded": {
                    "_text": "EmailAddress"
                }
            }, {
                "core:ContactNumberValue": {
                    "_text": order.billing_address.phone
                },
                "core:ContactNumberTypeCoded": {
                    "_text": "TelephoneNumber"
                }
            }]
        }
    }

    file.Order.OrderHeader.OrderParty['s0:SoldToParty']['core:NameAddress'] = {
        "core:Country": {
            "core:CountryCoded": {
                "_text": "Other"
            },
            "core:CountryCodedOther": {
                "_text": order.billing_address.country_iso2
            }
        },
        "core:Name1": {
            "_text": "Robin QC Testing"
        },
        "core:City": {
            "_text": cleanInput(order.billing_address.city)
        },
        "core:PostalCode": {
            "_text": order.billing_address.zip
        },
        "core:Region": {
            "core:RegionCoded": {
                "_text": "Other"
            },
            "core:RegionCodedOther": {
                "_text": order.billing_address.state
            }
        },
        "core:Street": {
            "_text": cleanInput(`${order.billing_address.street_1} ${order.billing_address.street_2}`)
        }
    }

    file.Order.OrderHeader.OrderParty['s0:SoldToParty']['core:PrimaryContact'] = {
        "core:ContactName": {
            "_text": cleanInput(`${order.billing_address.first_name} ${order.billing_address.last_name}`)
        },
        "core:ListOfContactNumber": {
            "core:ContactNumber": [{
                "core:ContactNumberTypeCoded": {
                    "_text": "EmailAddress"
                },
                "core:ContactNumberValue": {
                    "_text": order.billing_address.email
                }
            }, {
                "core:ContactNumberTypeCoded": {
                    "_text": "TelephoneNumber"
                },
                "core:ContactNumberValue": {
                    "_text": order.billing_address.phone
                }
            }]
        }
    }

    file.Order.OrderHeader.OrderParty['s0:SoldToParty']['core:PartyID']['core:Ident']._text = order_shipping.id;
    file.Order.OrderHeader.OrderParty['s0:SoldToParty']['core:Identifier']['core:Ident']._text = order_shipping.id;

    file.Order.OrderHeader.OrderPaymentInstructions['core:PaymentTerms']['core:PaymentTerm']['core:PaymentTermCodedOther']._text = order.payment_method;

    file.Order.OrderHeader['s0:OrderCurrency']['core:CurrencyCodedOther']._text = order.currency_code;

    // let's create the next array by looping through everything
    let value_set = [];

    // let's first create the HeaderReferences
    let pairs = [];
    let skip = ['billing_address', 'products', 'shipping_addresses', 'coupons', 'qr']
    for (let o in order) {
    	if (skip.indexOf(o) > -1) continue;
    	pairs.push({
    		'core:Name': {
    			_text: o
    		},
    		'core:Value': {
    			_text: order[o]
    		}
    	});
    }

    value_set.push({
        "core:SetName": {
            "_text": "HeaderReferences"
        },
        "core:SetID": {
            "_text": "Other"
        },
        "core:ListOfNameValuePair": {
            "core:NameValuePair": pairs
        }
    })

    // blank form fields
    value_set.push({
        "core:SetName": {
            "_text": "Form_Fields"
        },
        "core:ListOfNameValuePair": {}
    })

    // shipping address
    pairs = [];
    skip = ['shipping_quotes', 'billing_address']
    for (let o in order) {
    	if (skip.indexOf(o) > -1) continue;
    	pairs.push({
    		'core:Name': {
    			_text: o
    		},
    		'core:Value': {
    			_text: order[o]
    		}
    	});
    }

    value_set.push({
        "core:SetName": {
            "_text": "order_address"
        },
        "core:ListOfNameValuePair": {
            "core:NameValuePair": pairs
        }
    })

	file.Order.OrderHeader.ListOfNameValueSet['core:NameValueSet'] = value_set;

	// now let's create the products
	let items = [];
	for (let p in products) {
		let product = products[p];
		let prod = {
			"BaseItemDetail": {
                "LineItemNum": {
                    "core:BuyerLineItemNum": {
                        "_text": parseFloat(p) + 1
                    },
                    "core:SellerLineItemNum": {}
                },
                "ItemIdentifiers": {
                    "core:PartNumbers": {
                        "core:SellerPartNumber": {
                            "core:PartID": {
                                "_text": product.id.toString()
                            },
                            "core:PartIDExt": {
                                "_text": product.product_id.toString()
                            },
                            "core:RevisionNumber": {
                                "_text": product.order_address_id.toString()
                            }
                        },
                        "core:BuyerPartNumber": {
                            "core:PartID": {
                                "_text": product.sku.replace(/\.SET/g, '')
                            }
                        },
                        "core:OtherItemIdentifiers": {
                            "core:ProductIdentifierCoded": [{
                                "core:ProductIdentifierQualifierCoded": {
                                    "_text": "Color"
                                },
                                "core:ProductIdentifier": {}
                            }, {
                                "core:ProductIdentifierQualifierCoded": {
                                    "_text": "BuyersSizeCode"
                                },
                                "core:ProductIdentifier": {}
                            }]
                        }
                    },
                    "core:ItemDescription": {
                        "_text": product.name
                    },
                    "core:ListOfItemCharacteristic": {
                        "core:ItemCharacteristic": {
                            "core:ItemCharacteristicCodedOther": {
                                "_text": product.type
                            }
                        }
                    }
                },
                "TotalQuantity": {
                    "_attributes": {
                        "xmlns:xsi": "http://www.w3.org/2001/XMLSchema-instance",
                        "xsi:type": "core:QuantityType"
                    },
                    "core:UnitOfMeasurement": {
                        "core:UOMCoded": {
                            "_text": "Other"
                        },
                        "core:UOMCodedOther": {
                            "_text": "EA"
                        }
                    },
                    "core:QuantityValue": {
                        "_text": product.quantity.toString()
                    }
                },
                "MaxBackOrderQuantity": {
                    "core:QuantityValue": {}
                },
                "ListOfQuantityCoded": {
                    "core:QuantityCoded": {
                        "core:QuantityValue": {
                            "_text": "1"
                        },
                        "core:UnitOfMeasurement": {
                            "core:UOMCoded": {
                                "_text": "Other"
                            },
                            "core:UOMCodedOther": {
                                "_text": "EA"
                            }
                        },
                        "core:QuantityQualifierCoded": {
                            "_text": "Total"
                        }
                    }
                },
            },
            "PricingDetail": {
                "core:ListOfPrice": {
                    "core:Price": {
                        "core:UnitPrice": {
                            "core:UnitPriceValue": {
                                "_text": product.price_ex_tax
                            }
                        }
                    }
                },
                "core:Tax": {
                    "core:TaxTypeCoded": {
                        "_text": "Other"
                    },
                    "core:TaxTypeCodedOther": {
                        "core:Ident": {}
                    },
                    "core:TaxFunctionQualifierCoded": {
                        "_text": "Other"
                    },
                    "core:TaxFunctionQualifierCodedOther": {
                        "_text": "Other"
                    },
                    "core:TaxCategoryCoded": {
                        "_text": "Other"
                    },
                    "core:TaxCategoryCodedOther": {
                        "_text": "Other"
                    },
                    "core:ReasonTaxExemptCoded": {
                        "_text": "Other"
                    },
                    "core:ReasonTaxExemptCodedOther": {},
                    "core:TaxPercent": {
                    	"_text": product.active_tax_rate
                    },
                    "core:TaxAmount": {
                    	"_text": product.price_tax
                    },
                    "core:TaxLocation": {
                        "core:LocationQualifierCoded": {
                            "_text": "Other"
                        },
                        "core:LocationQualifierCodedOther": {},
                        "core:LocationIdentifier": {
                            "core:LocID": {
                                "core:Ident": {}
                            }
                        }
                    }
                },
                "core:LineItemTotal": {
                    "core:MonetaryAmount": {
                    	"_text": product.total_inc_tax
                    }
                }
            },
            "ListOfNameValueSet": {
            	"core:NameValueSet": []
            }
		}

		// we have to create an object of product details
		let pairs = [];
		let skip = ['product_options', 'applied_discounts', 'image_url', 'details', 'configurable_fields'];
		for(let d in product) {
			let detail = product[d];
			if (skip.indexOf(d) > -1) continue;
			pairs.push({
				'core:Name': {
	    			_text: d
	    		},
	    		'core:Value': {
	    			_text: detail
	    		}
			})
		}

		prod.ListOfNameValueSet['core:NameValueSet'].push({
			"core:SetName": {
				"_text": "DetailReferences",
			},
			"core:SetID": {
				"_text": "Other",
			},
			"core:ListOfNameValuePair": {
				"core:NameValuePair": pairs
			}
		});

		// an object of the order address
		pairs = [];
		skip = ['shipping_quotes', 'form_fields', 'billing_address'];
		for(let d in order_shipping) {
			let detail = order_shipping[d];
			if (skip.indexOf(d) > -1) continue;
			pairs.push({
				'core:Name': {
	    			_text: d
	    		},
	    		'core:Value': {
	    			_text: detail
	    		}
			})
		}

		prod.ListOfNameValueSet['core:NameValueSet'].push({
			"core:SetName": {
				"_text": "order_address",
			},
			"core:ListOfNameValuePair": {
				"core:NameValuePair": pairs
			}
		});

		// an object of the product options
		for(let o in product.product_options) {
			let option = product.product_options[o];
			pairs = [];
			skip = [];
			for(let d in option) {
				let detail = option[d];
				if (skip.indexOf(d) > -1) continue;
				pairs.push({
					'core:Name': {
		    			_text: d
		    		},
		    		'core:Value': {
		    			_text: detail
		    		}
				})
			}

			prod.ListOfNameValueSet['core:NameValueSet'].push({
				"core:SetName": {
					"_text": `option ${o}`,
				},
				"core:ListOfNameValuePair": {
					"core:NameValuePair": pairs
				}
			});
		}

		items.push(prod);
	}

	file.Order.OrderDetail.ListOfItemDetail.ItemDetail = items;

	// last a few header/summary items that need to be changed
	file.Order.OrderSummary['s0:OrderSubTotal']['core:MonetaryAmount']._text = order.subtotal_ex_tax;
	// file.Order.OrderSummary.OrderTotalTax['core:MonetaryAmount']._text = order.total_tax;
	// file.Order.OrderSummary.OrderDiscountTotal['core:MonetaryAmount']._text = order.coupon_discount;
	file.Order.OrderSummary.OrderTotal['core:MonetaryAmount']._text = order.total_inc_tax;
	
	file.Order['s0:orderHeader']['s0:OrderDates']['s0:ListOfDateCoded']['core:DateCoded']['core:DateQualifier']['core:Date']['core:Value'] = moment(order.date_created).format('YYYY-MM-DD HH:mm:ss')

	return file;
}

function cleanInput(input) {
	return input.replace(/'/g, '`');
}

function createFolders(folders, path, store, file) {
    return new Promise((resolve, reject) => {
        if (folders.length === 0) {
            path += "/";
            // once the folders are made we can move our file there
            let upload_file = "curl --user '"+store.dav_username+":"+store.dav_password+"'  -T '/tmp/"+file.name+"' '"+store.dav_path+"/content"+path+"/"+file.name+"' --anyauth";
            // console.log('upload_file', upload_file);
            exec(upload_file, (e, st, se) => {
                resolve(true);
            });
        } else {
            path += "/" + folders[0];
            // console.log('path', path);
            let mk_projectfolder = "curl --user '"+store.dav_username+":"+store.dav_password+"' -X MKCOL '"+store.dav_path+"/content"+path+"' --anyauth";
            // console.log('dav', mk_projectfolder);
            exec(mk_projectfolder, (e,st,se) => {
                // console.log("make folder", st);
                folders.shift();
                createFolders(folders, path, store, file).then(res => {
                    resolve(res);
                })
            });
        }
    });
}

function POSClass(store) {
	return init().then(() => {
		return {
			sendToEbridge: sendToEbridge,
			generateQRCode: generateQRCode
		}
	});
}

module.exports = POSClass;