const request = require('../services/request.service');
const config = require('../config/config');
const fs = require('fs');
const path = require('path');

// variables
let headers = {
	"Accept": "application/json",
    "Content-Type": "application/json",
    "X-Auth-Client": config.client_id,
    "X-Auth-Token": config.token
}

let allTotalOrders = '';
let allCategories = '';
let allProducts = '';
let allOrders = '';
let orderProductDetail = [];
let grandOrders = [];
let newGrandOrders = [];
const csvHeader = {
    OrderID: "Order ID",
    SubTotal: "SubTotal",
    TotalIncTax: "Total Inc Tax",
    PaymentStatus: "Payment Status",
    PaymentMethod: "Payment Method",
    PaymentProviderID: "Payment Provider ID",
    ItemsTotal: "Items total",
    ShippingCost: "Shipping Cost",
    DateShipped: "Date Shipped",
    IP: "IP",
    DateCreated: "Date Created",
    DateModified: "Date Modified",
    Status: "Status",
    CustomerMessage: "Customer Message",
    StaffNotes: "Staff Notes",
    CustomerFirstName: "Customer First Name",
    CustomerLastName: "Customer Last Name",
    CustomerEmail: "Customer Email",
    ProductID: "Product ID",
    ProductName: "Product Name",
    ProductSKU: "Product SKU",
    ProductCategory: "Product Category",
    ProductPrice: "Product Price",
    OrderQuantity: "Order Quantity"
};

async function getAllProducts() {
    try {
        const products = await request.get(`${config.bigCommerce}/${config.storeHash}/v3/catalog/products`, {}, headers, true);

        return products.data;
    }
    catch (error) {
        console.log('Error', error === 'string' ? error : 'Unable to complete getAllProducts');
	}
}

async function getAllCategories() {
    try {
        const categories = await request.get(`${config.bigCommerce}/${config.storeHash}/v3/catalog/categories`, {}, headers, true);

        return categories.data;
    }
    catch (error) {
        console.log('Error', error === 'string' ? error : 'Unable to complete getAllCategories');
	}
}


async function getProductCategories(categoryIds) {
    try {
        const categoryId = categoryIds[0];
        for (let index in allCategories) {
            if (categoryId === allCategories[index].id) {
                return allCategories[index].name;
            }
        }
    }
    catch (error) {
        console.log('Error', error === 'string' ? error : 'Unable to complete getProductCategories');
	}
}

async function getAllTotalOrders() {
    try {
        const totalOrders = await request.get(`${config.bigCommerce}/${config.storeHash}/v2/orders/count`, {}, headers, true);

        return totalOrders.count;
    }
    catch (error) {
        console.log('Error', error === 'string' ? error : 'Unable to complete getAllTotalOrders');
        console.log(error);
	}
}

// date will be used or not?
// ?limit=250&page=2
async function getAllOrders(pageNumber) {
    try {
        const orders = await request.get(`${config.bigCommerce}/${config.storeHash}/v2/orders?limit=250&page=${pageNumber}`, {}, headers, true);

        grandOrders.push(orders);
    }
    catch (error) {
        console.log('Error', error === 'string' ? error : 'Unable to complete getAllOrders');
        console.log(error);
	}
}

async function getListOrderProducts(orderId) {
    try {
        let productResult = '';
        let categoryData = '';
        let productDetail = '';
        const orderInfo = await getOrderDetail(orderId);
        const orderProducts = await request.get(`${config.bigCommerce}/${config.storeHash}/v2/orders/${orderId}/products`, {}, headers, true);

        if (orderProducts.length > 1) {
            for (let index in orderProducts) {
                productResult = allProducts.filter(obj => {
                    return obj.id === orderProducts[index].product_id;
                });

                console.log('A orderId:', orderId, 'product_id:', orderProducts[index].product_id);
                if (productResult.length !== 0) {
                    categoryData = await getProductCategories(productResult[0].categories);
                    productDetail = productResult[0];
                } else {
                    categoryData = 'n/a';
                    productDetail = {
                        id: orderProducts[index].product_id,
                        name: '',
                        sku: '',
                        price: ''
                    };
                }

                orderDetail = {
                    order: orderInfo,
                    orderQuantity: orderProducts[index].quantity,
                    detail: productDetail,
                    category: categoryData
                };

                orderProductDetail.push(orderDetail);
            }
        } else {
            productResult = allProducts.filter(obj => {
                return obj.id === orderProducts[0].product_id;
            });

            console.log('B orderId:', orderId, 'product_id:', orderProducts[0].product_id);
            if (typeof productResult[0] !== 'undefined') {
                categoryData = await getProductCategories(productResult[0].categories);
                productDetail = productResult[0];
            } else {
                categoryData = 'n/a';
                productDetail = {
                    id: orderProducts[0].product_id,
                    name: '',
                    sku: '',
                    price: ''
                };
            }

            orderDetail = {
                order: orderInfo,
                orderQuantity: orderProducts[0].quantity,
                detail: productDetail,
                category: categoryData
            };

            orderProductDetail.push(orderDetail);
        }
    }
    catch (error) {
        console.log('Error', error === 'string' ? error : 'Unable to complete getListOrderProducts');
        console.log(error);
	}
}

async function getAllOrderDetailTogether() {
    try {
        for (let index in newGrandOrders) {
            await getListOrderProducts(newGrandOrders[index].id);
        }
    }
    catch (error) {
        console.log('Error', error === 'string' ? error : 'Unable to complete getAllOrderDetail');
        console.log(error);
	}
}

async function getOrderDetail(orderId) {
    try {
         const orderResult = newGrandOrders.filter(obj => {
            return obj.id === orderId;
         });

        return orderResult[0];
    }
    catch (error) {
        console.log('Error', error === 'string' ? error : 'Unable to complete getOrderDetail');
        console.log(error);
	}
}

async function processCsv(req, res) {
    try {
        allTotalOrders = await getAllTotalOrders();
        allProducts = await getAllProducts();
        allCategories = await getAllCategories();

        let numberOfPages = Math.ceil(allTotalOrders / 250)

        console.log('Start Total Orders:', allTotalOrders);
        console.log('Total Pages:', numberOfPages);
        console.log('allProducts:', allProducts.length);
        console.log('allCategories:', allCategories.length);
        numberOfPages = 2;

        for (let i = 0; i < numberOfPages; i += 1) {
            console.log('Page: ', i + 1);
            await getAllOrders(i + 1);
        }

        console.log('Total Group Order:', grandOrders.length);

        for (let i = 0; i < grandOrders.length; i+= 1) {
            newGrandOrders = newGrandOrders.concat(grandOrders[i]);
        }

        console.log('Total new Group Order:', newGrandOrders.length);

        await getAllOrderDetailTogether();

        console.log('Total order detail:', orderProductDetail.length);
        let itemsFormatted = [];
        orderProductDetail.forEach((item) => {
            itemsFormatted.push({
                OrderID: item.order.id || '',
                SubTotal: item.order.subtotal_ex_tax || '',
                TotalIncTax: item.order.total_inc_tax || '',
                PaymentStatus: item.order.payment_status || '',
                PaymentMethod: item.order.payment_method || '',
                PaymentProviderID: item.order.payment_provider_id || '',
                ItemsTotal: item.order.items_total || '',
                ShippingCost: item.order.shipping_cost_inc_tax || '',
                DateShipped: sanitizeData(item.order.date_shipped),
                IP: item.order.ip_address || '',
                DateCreated: sanitizeData(item.order.date_created),
                DateModified: sanitizeData(item.order.date_modified),
                Status: item.order.status || '',
                CustomerMessage: sanitizeData(item.order.customer_message),
                StaffNotes: sanitizeData(item.order.staff_notes),
                CustomerFirstName: sanitizeData(item.order.billing_address.first_name),
                CustomerLastName: sanitizeData(item.order.billing_address.last_name),
                CustomerEmail: item.order.billing_address.email || '',
                ProductID: item.detail.id,
                ProductName: sanitizeData(item.detail.name) || '',
                ProductSKU: sanitizeData(item.detail.sku) || '',
                ProductCategory: sanitizeData(item.category) || '',
                ProductPrice: sanitizeData(item.detail.price) || '',
                OrderQuantity: item.orderQuantity
            });
        });

        itemsFormatted.unshift(csvHeader);
        const jsonObject = JSON.stringify(itemsFormatted);

        const csvData = convertToCSV(jsonObject);
        const filename = path.join(path.parse(process.cwd()).dir, 'output.csv');

        fs.writeFileSync(filename, csvData);

        console.log('File Location:', filename);
        res.status(400).send({
            message: 'Total Order:' + allTotalOrders
        });
        res.end();
    }
    catch (error) {
        // catch the error
        console.log('Error', error === 'string' ? error : 'Unable to complete the process');
        console.log(error);

        res.status(403).send({
            message: 'Unable to complete the process'
        });
        res.end();
	}
}

function shortById(a, b) {
    const genreA = a.id;
    const genreB = b.id;

    let comparison = 0;
    if (genreA > genreB) {
      comparison = 1;
    } else if (genreA < genreB) {
      comparison = -1;
    }

    return comparison;
}

function convertToCSV(objArray) {
    var array = typeof objArray != 'object' ? JSON.parse(objArray) : objArray;
    var str = '';

    for (var i = 0; i < array.length; i++) {
        var line = '';
        for (var index in array[i]) {
            if (line != '') line += ','

            line += array[i][index];
        }

        str += line + '\r\n';
    }

    return str;
}

function sanitizeData(data) {
    if (typeof data !== 'undefined') {
        if (typeof data === 'string') {
            data = data.replace(/(\r\n|\n|\r)/gm, "");
            return data.replace(/,/g, '');
        }

        return data;
    }
}

module.exports = {
    processCsv: processCsv
};
