const request = require('../services/request.service');
let config = require('../config/config');

async function init(store) {
	// if (config.data_token) return false;

	// when this function initiates, we have to get the token 
	let data = await request.get(`${config.ribon}/api/store/${store}/services/tokens`, {}, {}, true);

	if (data && data.length > 0 && data[0].url) {
		// let's go through all the items in the data
		data.forEach(token => {
			config[`${token.name}_token`] = token.token;
		});

		if (!config.data_token)
			throw (`Data token is not present on store ${store}`);
	} else {
		// throw an error if the token doesn't exist
		console.log("data", data);
		throw (`Unable to get token for store - ${store}`);
	}

	console.log('data_token', config.data_token);
	return config.data_token;
}

async function getData(table, query) {
	let data = await request.get(`${config.ribon_data}/api/v1/data/${table}`, query, { token: config.data_token }, true);
	console.log('data', `${config.ribon_data}/api/v1/data/${table}`, query, config.data_token );

	return data;
}

async function postData(table, query) {
	let id = await request.post(`${config.ribon_data}/api/v1/data/${table}`, query, { token: config.data_token }, true);

	return id;
}

async function updateData(table, id, query) {
	let update = await request.post(`${config.ribon_data}/api/v1/data/${table}/${id}`, query, { token: config.data_token }, true);

	return update;
}

function DataClass(store) {
	return init(store).then(() => {
		return {
			getData: getData,
			postData: postData,
			updateData: updateData
		}
	});
}

module.exports = DataClass;