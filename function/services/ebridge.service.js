const soap  = require('soap');
const request = require('../services/request.service');
const xmljs = require('xml-js');
const url = "https://www.ebridgeservices.com/eportalservice.asmx?wsdl";
const username = "QAPI60454";
const password = "492841";
let ebridge = false;

function init() {
	// connect with the client
	return new Promise((resolve, reject) => {
		soap.createClient(url, { login: username, password: password }, function(err, client) {
			// console.log('client', err, client);
			if (err) {
				reject('Unable to connect to ebridge');
				return false;
			} else {
				ebridge = client;
				resolve(client);
			}
		});
	});
	
}

function getDoc(id) {
	return new Promise((resolve, reject) => {
		i
		ebridge.ePortalService.ePortalServiceSoap.GetDocument({ login: username, password: password, sys_no: id }, (err, client) => {
			// console.log('send response', err, client.GetDocumentResult);
			client = JSON.parse(xmljs.xml2json(client.GetDocumentResult, { compact: true }));
			// console.log('client', client);
			resolve(client);
		});
	});
}

function postDocument(content, filename) {
	return new Promise((resolve, reject) => {
		ebridge.ePortalService.ePortalServiceSoap.SendFile({ login: username, password: password, content: content, filename: filename }, (err, client) => {
			// console.log('send response', client);
			resolve(client);
		});
	});
}

function EbridgeClass() {
	return init().then(() => {
		return {
			getDoc: getDoc,
			postDocument: postDocument
		}
	});
}

module.exports = EbridgeClass;