const request = require('../services/request.service');
let config = require('../config/config');

let mailer_token = false;

async function init(store) {
	// if (config.mailer_token) return false;

	// when this function initiates, we have to get the token 
	let data = await request.get(`${config.ribon}/api/store/${store}/services/tokens`, {}, {}, true);

	if (data && data.length > 0 && data[0].url) {
		// let's go through all the items in the data
		data.forEach(token => {
			config[`${token.name}_token`] = token.token;
		});

		if (!config.mailer_token)
			throw (`Data token is not present on store ${store}`);
	} else {
		// throw an error if the token doesn't exist
		console.log("data", data);
		throw (`Unable to get token for store - ${store}`);
	}

	console.log('mailer_token', config.mailer_token);
	return config.mailer_token;
}

async function sendMail(object) {
	let headers = {
		Authorization: `Bearer ${config.mailer_token}`, 
		'Content-Type': 'application/json', 
		'Accept': 'application/json'
	}

	let response = false;

	try {
		// there's a slight bug with the mailer where certain errors are actually successful sends
		// console.log('mailer headers', headers, config);
		await request.post(`${config.ribon_mailer}/api/v1/mailer`, object, headers, true);

		// by default the mailer doesn't send anything back so we can assume a true unless we get an error
		response = true;
	}
	catch (e) {
		console.log('mailer r', e.message, e.name);
		if (e.message && e.message.indexOf('412') > -1) {
			response = true;
		} else {
			throw('mail not sent');
		}
	}

	return response;
}

function MailerClass(store) {
	return init(store).then(() => {
		return {
			sendMail: sendMail,
		}
	});
}

module.exports = MailerClass;