const http = require('https');
const request = require('request-promise');

function get(url, query, headers, json) {
	let options = {
		rejectUnauthorized: false, 
		url: url,
		method: 'GET',
		headers: headers,
		qs: query,
		json: json
	}
	return request(options);
	// return new Promise((resolve, reject) => {
	// 	http.get(url, function(res) { 
	//         res.setEncoding('utf8');
	// 		res.on('data', function (chunk) {
	// 			// console.log('BODY: ' + chunk);
	// 			if (json) {
	// 				resolve(JSON.parse(chunk));
	// 			} else {
	// 				resolve(chunk);
	// 			}
	// 		});
	//     }).on('error', function(e) { 
	//         reject(res);
	//     }); 
	// })
}

function post(url, query, headers, json) {
	let options = {
		rejectUnauthorized: false, 
		url: url,
		method: 'POST',
		headers: headers,
		body: query,
		json: json
	}
	return request(options);
}

function put(url, query, headers, json) {
	let options = {
		rejectUnauthorized: false, 
		url: url,
		method: 'PUT',
		headers: headers,
		body: query,
		json: json
	}
	return request(options);
}

function deleteReq(url, query, headers, json) {
	let options = {
		rejectUnauthorized: false, 
		url: url,
		method: 'DELETE',
		headers: headers,
		qs: query,
		json: json
	}
	return request(options);
}

module.exports.get = get;
module.exports.post = post;
module.exports.put = put;
module.exports.delete = deleteReq;