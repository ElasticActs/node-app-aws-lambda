/**
 * Live Function
 *
 * When pushing live, this needs to be uncommented
 */
if (process.env.ENV && process.env.ENV === 'live') {
	/**
	 * AWS Lambda Setup
	 *
	 */
	const config = require('./function/config/config');
	const app = require('./function/config/express');
	const awsServerlessExpress = require('aws-serverless-express');

	const server = awsServerlessExpress.createServer(app);
	module.exports.handler = (event, context) => awsServerlessExpress.proxy(server, event, context);
} else {
	/**
	 * When testing your function on your local node server
	 * uncomment the code below
	 */
	const config = require('./function/config/config');
	const app = require('./function/config/express');
	const http = require('http');
	const https = require('https');
	const fs = require('fs');

	let options = {
		key: fs.readFileSync('server.key'),
		cert: fs.readFileSync('server.crt')
	};
	http.createServer(app).listen(config.port, () => {
		console.log('http server connected');
	});
	https.createServer(options, app).listen(443, () => {
		console.log('ssl server connected');
	});
}
